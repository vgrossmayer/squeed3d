extends RigidBody2D

class_name Cube

func _ready() -> void:
	$AnimationPlayer.play("spawn")

func _integrate_forces(state: Physics2DDirectBodyState) -> void:
	linear_velocity = Vector2.LEFT * Game.speed * 10

func _on_collide(body: Node) -> void:
	pass

func _on_body_entered(body: Node) -> void:
	_on_collide(body)
	$AnimationPlayer.play("die")
