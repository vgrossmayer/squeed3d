v 0.1

Controls:
WASD or Arrows

Blocks:
* blue: damage and slow
* red: double damage
* green: speed up
* yellow: x10 points

Multiplier increases points gained. Blocks bring 1 point.