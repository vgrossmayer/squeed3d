extends Node

func _ready():
	spawn()
	
func spawn() -> void:
	var r := randi()
	for i in get_child_count():
		if int(pow(2, i)) & r:
			get_child(i).spawn()
	yield(get_tree().create_timer(1), "timeout")
	print("spawn")
	spawn()
