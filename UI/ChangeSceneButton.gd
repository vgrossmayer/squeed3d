extends Button

export(PackedScene) var scene



func _on_pressed() -> void:
	if scene:
		get_tree().change_scene_to(scene)
