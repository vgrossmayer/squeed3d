extends TextureProgress

func _process(delta: float) -> void:
	
	value += delta * Game.multiplier_delta
	if value == 100.0:
		value = 0
		Game.multiplier += 1
