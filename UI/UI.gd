extends Control

onready var speed_progress := $Speed
onready var multiplier_progress := $MarginContainer/Multiplier
onready var multiplier_label := $MarginContainer/Label
onready var points_label := $Points

func _ready():
	Game.connect("speed_changed", self, "set_speed")
	Game.connect("multiplier_changed", self, "set_multiplier")
	Game.connect("points_changed", self, "set_points")


func set_speed(new_speed: float) -> void:
	speed_progress.value = new_speed
	var speed_color: Color = lerp(Game.current_colors[0], Game.current_colors[1], Game.current_interval_ratio)
	modulate = speed_color


func set_multiplier(new_multiplier: int) -> void:
	multiplier_label.text = ("x%d" % new_multiplier)
	multiplier_progress.value = 0


func set_points(new_points: int) -> void:
	points_label.text = ("%d" % new_points)


func _on_SpeedSlider_value_changed(value: float) -> void:
	Game.speed = value
