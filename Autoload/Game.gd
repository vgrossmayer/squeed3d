extends Node

signal speed_changed(new_speed)
signal multiplier_changed(new_value)
signal points_changed(new_points)
signal health_changed(new_health, new_sprite)

var speed : float = 1 setget set_speed
const speed_intervals := [0.0, 50.0, 70.0, 100.0]
const speed_colors := PoolColorArray([
	Color(0.43, 0.74, 1),
	Color(1, 0.43,0.43),
	Color(0.95, 1, 0.43),
	Color(0.43, 1, 0.57)
	]
)

var current_colors: PoolColorArray setget ,get_current_colors
var current_interval_ratio: float setget ,get_current_interval_ratio
var current_sprite: Texture setget ,get_current_sprite

var multiplier: int = 1 setget set_multiplier
var multiplier_delta: float setget ,get_multiplier_delta

var points: int = 0 setget set_points
var health: int = 10 setget set_health

var hyperspeed_available: bool = false
var hyperspeed_active: bool = false

func get_current_sprite() -> Texture:
	return load("res://assets/square_break/square_break_%d.png" % (10 - health)) as Texture
"res://assets/square_break/square_break_0.png"

func set_health(new_health: int) -> void:
	health = new_health
	if health < 0:
		die()
	else:
		emit_signal("health_changed", health, self.current_sprite)


func get_multiplier_delta() -> float:
	var index: int = 1
	for i in speed_intervals:
		if i >= speed:
			index = speed_intervals.find(i)
			break
	if index == speed_colors.size() - 1:
		return 8.0
	if index == speed_colors.size() - 2:
		return 4.0
	if index == speed_colors.size() - 3:
		return 2.0
	return 0.0

func get_current_colors() -> PoolColorArray:
	var index: int = 1
	for i in speed_intervals:
		if i >= speed:
			index = speed_intervals.find(i)
			break
	return PoolColorArray([speed_colors[max(0, index - 1)], speed_colors[index]])


func get_current_interval_ratio() -> float:
	var index: int = 1
	for i in speed_intervals:
		if i >= speed:
			index = speed_intervals.find(i)
			break
	return (speed - speed_intervals[max(0, index - 1)]) / (speed_intervals[index] - speed_intervals[max(0, index - 1)])
	
func set_points(new_points: int) -> void:
	var delta = new_points - points
	if delta > 0:
		delta *= multiplier
	points = max(points + delta, 0)
	emit_signal("points_changed", points)


func set_multiplier(new_value: int) -> void:
	multiplier = max(new_value, 1)
	emit_signal("multiplier_changed", multiplier)


func set_speed(new_speed: float) -> void:
	speed = clamp(new_speed, 0.1, 100.0)
	emit_signal("speed_changed", speed)


func _ready() -> void:
	set_process(true)

func _process(delta: float) -> void:
	self.speed += (3 * delta)


func start() -> void:
	speed = 1
	multiplier = 1
	points = 0
	health = 10


func die() -> void:
	get_tree().change_scene("res://UI/GameOver.tscn")
