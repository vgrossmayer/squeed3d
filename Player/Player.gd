extends RigidBody2D

export(float, 0, 1000) var JUMP_FORCE := 0.0

var speed: float = 1

onready var particles := $CPUParticles2D

func _ready():
	Game.connect("speed_changed", self, "set_speed")
	Game.connect("health_changed", self, "set_sprite")
	$AnimationPlayer.play("Idle")

func set_sprite(_new_health: int, sprite: Texture) -> void:
	$square.texture = sprite


func set_speed(new_speed: float) -> void:
	if particles and new_speed != speed:
		particles.lifetime = 2.0 + 2.0 * (new_speed / 100)
#		particles.initial_velocity = (Game.speed) * 10
		particles.speed_scale = 2.0 + 2.0 * (new_speed / 100)
		particles.explosiveness = 0.3 - (new_speed / 300)
		particles.emission_sphere_radius = 16.0 - 8.0 * (new_speed / 100)
		
		$AnimationPlayer.playback_speed = 1.0 + 9.0 * (new_speed / 100)
	speed = new_speed

func _integrate_forces(state: Physics2DDirectBodyState) -> void:
	if state.transform.get_origin().distance_to(Vector2(415, 300)) < 5:
		state.linear_velocity *= 0.6
	if Input.is_action_just_pressed("jump_down"):
		state.linear_velocity.y = 0
		state.apply_central_impulse(Vector2.DOWN * JUMP_FORCE)
	if Input.is_action_just_pressed("jump_up"):
		state.linear_velocity.y = 0
		state.apply_central_impulse(Vector2.UP * JUMP_FORCE)
	if Input.is_action_just_pressed("slow_down"):
		state.linear_velocity = Vector2.ZERO
	if Input.is_action_pressed("slow_down"):
		gravity_scale = 0.01
	else:
		gravity_scale = 1.0


func _on_Player_body_entered(body: Node) -> void:
	if body.is_in_group("death"):
		Game.die()
	pass # Replace with function body.
