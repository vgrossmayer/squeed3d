extends Position2D

export(Array, PackedScene) var cubes
export(Curve) var speed_curve: Curve
export(Curve) var type_curve: Curve

var times = [3, 4, 5, 6, 7, 8, 9 ,10, 11, 12]

func _ready() -> void:
	randomize()

func spawn() -> void:
	if cubes:
		var r: int = round(type_curve.interpolate(randf()))
		print(r)
		var new_cube = cubes[r].instance()
		add_child(new_cube)


func _on_Timer_timeout() -> void:
	var r = speed_curve.interpolate(Game.speed / 100.0)
	$Timer.start(r * 2.8)
	if randf() > (0.3 + 0.4 * r):
		spawn()
